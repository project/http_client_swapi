operations:
  PagedFilms:
    summary: Get a paged list of films
    httpMethod: GET
    uri: films
    responseModel: PagedFilms
    parameters:
      search:
        type: string
        location: query
        description: >-
          Filters the set of resources returned. Search fields: title.
  FilmById:
    summary: Get a specific film resource
    httpMethod: GET
    uri: films/{id}
    responseModel: Film
    parameters:
      id:
        type: string
        location: uri
        description: The ID of the resource
        required: true

models:
  Film:
    type: object
    properties:
      title:
        type: string
        description: The title of this film.
        location: json
      episode_id:
        type: integer
        description: The episode number of this film.
        location: json
      opening_crawl:
        type: string
        description: The opening paragraphs at the beginning of this film.
        location: json
      director:
        type: string
        description: The name of the director of this film.
        location: json
      producer:
        type: string
        description: >-
          The name(s) of the producer(s) of this film. Comma separated.
        location: json
      release_date:
        type: string
        description: >-
          The ISO 8601 date format of film release at original creator country.
        location: json
      species:
        type: array
        description: An array of species resource URLs that are in this film.
        location: json
        items:
          type: string
      starships:
        type: array
        description: An array of starship resource URLs that are in this film.
        location: json
        items:
          type: string
      vehicles:
        type: array
        description: An array of vehicle resource URLs that are in this film.
        location: json
        items:
          type: string
      characters:
        type: array
        description: An array of people resource URLs that are in this film.
        location: json
        items:
          type: string
      planets:
        type: array
        description: An array of planet resource URLs that are in this film.
        location: json
        items:
          type: string
      url:
        type: string
        description: The hypermedia URL of this resource.
        location: json
      created:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was created.
        location: json
      edited:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was edited.
        location: json
  PagedFilms:
    type: object
    properties:
      count:
        type: number
        description: The total number results.
        location: json
      next:
        type: string
        description: The URL of the next page of results.
        location: json
      previous:
        type: string
        description: The URL of the previous page of results.
        location: json
      results:
        type: array
        description: An array of films.
        location: json
        items:
          "$ref": "Film"
