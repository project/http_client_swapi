operations:
  PagedPeople:
    summary: Get a paged list of people
    httpMethod: GET
    uri: people
    responseModel: PagedPeople
    parameters:
      search:
        type: string
        location: query
        description: >-
          Filters the set of resources returned. Search fields: name.
  PersonById:
    extends: FilmById
    summary: Get a specific person resource
    uri: people/{id}
    responseModel: Person

models:
  Person:
    type: object
    properties:
      name:
        type: string
        description: The name of this person.
        location: json
      birth_year:
        type: string
        description: >-
          The birth year of the person, using the in-universe standard of BBY or
          ABY - Before the Battle of Yavin or After the Battle of Yavin. The
          Battle of Yavin is a battle that occurs at the end of Star Wars
          episode IV: A New Hope.
        location: json
      eye_color:
        type: string
        description: >-
          The eye color of this person. Will be "unknown" if not known or "n/a"
          if the person does not have an eye.
        location: json
      gender:
        type: string
        description: >-
          The gender of this person. Either "Male", "Female" or "unknown", "n/a"
          if the person does not have a gender.
        location: json
      hair_color:
        type: string
        description: >-
          The hair color of this person. Will be "unknown" if not known or "n/a"
          if the person does not have hair.
        location: json
      height:
        type: string
        description: The height of the person in centimeters.
        location: json
      mass:
        type: string
        description: The mass of the person in kilograms.
        location: json
      skin_color:
        type: string
        description: The skin color of this person.
        location: json
      homeworld:
        type: string
        description: >-
          The URL of a planet resource, a planet that this person was born on or
          inhabits.
        location: json
      films:
        type: array
        description: >-
          An array of film resource URLs that this person has been in.
        location: json
        items:
          type: string
      species:
        type: array
        description: >-
          An array of species resource URLs that this person belongs to.
        location: json
        items:
          type: string
      starships:
        type: array
        description: >-
          An array of starship resource URLs that this person has piloted.
        location: json
        items:
          type: string
      vehicles:
        type: array
        description: >-
          An array of vehicle resource URLs that this person has piloted.
        location: json
        items:
          type: string
      url:
        type: string
        description: The hypermedia URL of this resource.
        location: json
      created:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was created.
        location: json
      edited:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was edited.
        location: json
  PagedPeople:
    type: object
    properties:
      count:
        type: number
        description: The total number results.
        location: json
      next:
        type: string
        description: The URL of the next page of results.
        location: json
      previous:
        type: string
        description: The URL of the previous page of results.
        location: json
      results:
        type: array
        description: An array of people.
        location: json
        items:
          "$ref": "Person"
