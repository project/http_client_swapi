operations:
  PagedPlanets:
    summary: Get a paged list of planets
    httpMethod: GET
    uri: planets
    responseModel: PagedPlanets
    parameters:
      search:
        type: string
        location: query
        description: >-
          Filters the set of resources returned. Search fields: name.
  PlanetById:
    extends: FilmById
    summary: Get a specific planet resource
    uri: planets/{id}
    responseModel: Planet

models:
  Planet:
    type: object
    properties:
      name:
        type: string
        description: The name of this planet.
        location: json
      diameter:
        type: string
        description: The diameter of this planet in kilometers.
        location: json
      rotation_period:
        type: string
        description: >-
          The number of standard hours it takes for this planet to complete a
          single rotation on its axis.
        location: json
      orbital_period:
        type: string
        description: >-
          The number of standard days it takes for this planet to complete a
          single orbit of its local star.
        location: json
      gravity:
        type: string
        description: >-
          A number denoting the gravity of this planet, where "1" is normal or 1
          standard G. "2" is twice or 2 standard Gs. "0.5" is half or 0.5
          standard Gs.
        location: json
      population:
        type: string
        description: >-
          The average population of sentient beings inhabiting this planet.
        location: json
      climate:
        type: string
        description: The climate of this planet. Comma separated if diverse.
        location: json
      terrain:
        type: string
        description: The terrain of this planet. Comma separated if diverse.
        location: json
      surface_water:
        type: string
        description: >-
          The percentage of the planet surface that is naturally occurring water
          or bodies of water.
        location: json
      residents:
        type: array
        description: An array of People URL Resources that live on this planet.
        location: json
        items:
          type: string
      films:
        type: array
        description: >-
          An array of Film URL Resources that this planet has appeared in.
        location: json
        items:
          type: string
      url:
        type: string
        description: The hypermedia URL of this resource.
        location: json
      created:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was created.
        location: json
      edited:
        type: string
        description: >-
          The ISO 8601 date format of the time that this resource was edited.
        location: json
  PagedPlanets:
    type: object
    properties:
      count:
        type: number
        description: The total number results.
        location: json
      next:
        type: string
        description: The URL of the next page of results.
        location: json
      previous:
        type: string
        description: The URL of the previous page of results.
        location: json
      results:
        type: array
        description: An array of planets.
        location: json
        items:
          "$ref": "Planet"
