# Star Wars API - HTTP Client

An [HTTP Client Manager][1] implementation of the [Star Wars API][2]. Initially
created as an example, it is a fully operational API client.

## Usage

For use in PHP, retrieve the client from the Drupal service registry, e.g.:

    $client = \Drupal::service('http_client_swapi.client');

Client operations can be invoked directly, e.g.:

    $client->personById(['id' => 1]);

For non-code usage, all operations are exposed as Drupal action plugins by
[HTTP Client Manager][1] so they can be called from any module that supports
actions like [ECA][3].

## Requirements

This module requires the following modules:

- [HTTP Client Manager][1]

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules][4].

## Configuration

This module has no menu or modifiable settings of it's own. Usage and
configuration instructions can be found in the docs for [HTTP Client Manager][1].

## Maintainers

- Brandon Williams - [rocketeerbkw][5]


[1]: https://www.drupal.org/project/http_client_manager
[2]: https://swapi.dev/
[3]: https://www.drupal.org/project/eca
[4]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
[5]: https://www.drupal.org/u/rocketeerbkw
